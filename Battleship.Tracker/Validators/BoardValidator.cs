﻿using FluentValidation;
using FluentValidation.Results;
using System.Linq;

namespace Battleship.Tracker.Validators
{
    //TODO - Validation for overlapping ships.
    public class BoardValidator : AbstractValidator<Models.Board>
    {
        public BoardValidator()
        {
            RuleFor(b => b.Rows).GreaterThan(0);
            RuleFor(b => b.Columns).GreaterThan(0);

            RuleFor(b => b.Ships).NotNull();
            RuleFor(b => b.Ships).NotEmpty();
            
            RuleFor(b => b.Ships).Custom((ships, context) =>
            {
                var maxRow = context.InstanceToValidate.Rows;
                var maxColumn = context.InstanceToValidate.Columns;

                foreach (var eachShip in ships)
                {
                    var outsideBoard = eachShip
                    .Positions
                    .Any(p => p.Row > maxRow || p.Column > maxColumn);

                    if (outsideBoard)
                        context.AddFailure(new ValidationFailure(
                            "Ships",
                            "Ship position outside the board",
                            eachShip));
                }
            });
        }
    }
}
