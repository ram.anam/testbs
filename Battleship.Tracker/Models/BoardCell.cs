﻿using System;

namespace Battleship.Tracker.Models
{
    public class BoardCell : IEquatable<BoardCell>
    {
        public int Row { get; private set; }
        public int Column { get; private set; }

        public BoardCell(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public bool Equals(BoardCell other)
        {
            return other != null && 
                   other.Column == Column && 
                   other.Row == Row;
        }
    }
}
