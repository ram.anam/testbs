﻿using System.Collections.Generic;
using System.Linq;

namespace Battleship.Tracker.Models
{
    public class BattleShip
    {
        private readonly List<BoardCell> _positions;
        public IReadOnlyList<BoardCell> Positions => _positions;

        private bool _sunk;
        public bool Sunk => _sunk;

        private readonly bool[] _attackedPositions;

        public BattleShip(
            BoardCell startCell,
            int size,
            ShipOrientation orientation)
        {
            var indexIncValue = GetIndexIncValue(orientation);

            var positions = new List<BoardCell>();
            for (int i = 0; i < size; i++)
            {
                var rowPos = startCell.Row + (indexIncValue.Item1 * i);
                var colPos = startCell.Column + (indexIncValue.Item2 * i);
                positions.Add(new BoardCell(rowPos, colPos));
            }

            _positions = positions;
            _attackedPositions = new bool[positions.Count]; 
        }

        public AttackResult Attack(BoardCell attackPosition)
        {
            var matchingPosition = _positions.IndexOf(attackPosition);
            
            if(matchingPosition == -1 || AlreadyAttacked(matchingPosition))
                return AttackResult.Miss;

            _attackedPositions[matchingPosition] = true;
            _sunk = _attackedPositions.All(ap => ap);
            return AttackResult.Hit;
        }

        private bool AlreadyAttacked(int position) => _attackedPositions[position];

        private (int, int) GetIndexIncValue(ShipOrientation orientation)
        {
            switch (orientation)
            {
                default:
                case ShipOrientation.Horizontal_LeftToRight:
                    return (0, 1);
                case ShipOrientation.Horizontal_RightToLeft:
                    return (0, -1);
                case ShipOrientation.Vertical_TopToBottom:
                    return (-1, 0);
                case ShipOrientation.Vertical_BottomToTop:
                    return (1, 0);
            }
        }
    }
}