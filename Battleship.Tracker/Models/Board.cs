﻿using System.Collections.Generic;
using System.Linq;

namespace Battleship.Tracker.Models
{
    public class Board
    {
        private readonly IReadOnlyList<BattleShip> _ships;
        public IReadOnlyList<BattleShip> Ships => _ships;

        public int Rows { get; private set; }
        public int Columns { get; private set; }

        public Board(IReadOnlyList<BattleShip> ships,
                     int rows = 10,
                     int columns = 10)
        {
            Rows = rows;
            Columns = columns;
            _ships = new List<BattleShip>(ships);
        }

        public AttackResult Attack(BoardCell attackPosition)
        {
            return _ships.Any(s => s.Attack(attackPosition) == AttackResult.Hit) ?
                AttackResult.Hit :
                AttackResult.Miss;
        }

        public bool HasLost() => _ships.All(s => s.Sunk);
    }
}
