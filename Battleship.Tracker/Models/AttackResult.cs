﻿namespace Battleship.Tracker.Models
{
    public enum AttackResult
    {
      Hit,
      Miss
    }
}
