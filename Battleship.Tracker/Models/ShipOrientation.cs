﻿namespace Battleship.Tracker.Models
{
    public enum ShipOrientation
    {
        Horizontal_LeftToRight,
        Horizontal_RightToLeft,
        Vertical_TopToBottom,
        Vertical_BottomToTop,
    }
}
