﻿using Battleship.Tracker.Models;
using Shouldly;
using System.Collections.Generic;
using Xunit;

namespace Battleship.Tracker.Tests.Models
{
    public class ShipBuilderParameters
    {
        public ShipOrientation ShipOrientation { get; set; }
        public int Size { get; set; }
        public BoardCell StartCell { get; set; }
        public List<BoardCell> ExpectedPositions { get; set; }
    }

    public class ShipInitializeTests
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void Given_Valid_Inputs_When_Ship_Then_Has_Valid_Positions(
            ShipBuilderParameters testParameters)
        {
            // Act.
            var ship = new BattleShip(testParameters.StartCell, testParameters.Size, testParameters.ShipOrientation);

            // Assert.
            testParameters.ExpectedPositions.Count.ShouldBe(ship.Positions.Count);

            for (int i = 0; i < testParameters.ExpectedPositions.Count; i++)
            {
                ship.Positions[i].Row.ShouldBe(testParameters.ExpectedPositions[i].Row);
                ship.Positions[i].Column.ShouldBe(testParameters.ExpectedPositions[i].Column);
            }
        }

        public static IEnumerable<object[]> TestData()
        {
            yield return new ShipBuilderParameters[] {new ShipBuilderParameters
                {
                    StartCell = new BoardCell(1,1),
                    Size = 3,
                    ShipOrientation = ShipOrientation.Horizontal_LeftToRight,
                    ExpectedPositions = new List<BoardCell>
                    {
                        new BoardCell(1,1),
                        new BoardCell(1,2),
                        new BoardCell(1,3)
                    }
                } 
            };

            yield return new ShipBuilderParameters[] {new ShipBuilderParameters
                {
                    StartCell = new BoardCell(1,5),
                    Size = 3,
                    ShipOrientation = ShipOrientation.Horizontal_RightToLeft,
                    ExpectedPositions = new List<BoardCell>
                    {
                        new BoardCell(1,5),
                        new BoardCell(1,4),
                        new BoardCell(1,3)
                    }
                } 
            };

            yield return new ShipBuilderParameters[] { new ShipBuilderParameters
                {
                    StartCell = new BoardCell(3,2),
                    Size = 3,
                    ShipOrientation = ShipOrientation.Vertical_TopToBottom,
                    ExpectedPositions = new List<BoardCell>
                    {
                        new BoardCell(3,2),
                        new BoardCell(2,2),
                        new BoardCell(1,2)
                    }
                }
            };

            yield return new ShipBuilderParameters[] { new ShipBuilderParameters
                {
                    StartCell = new BoardCell(1,1),
                    Size = 3,
                    ShipOrientation = ShipOrientation.Vertical_BottomToTop,
                    ExpectedPositions = new List<BoardCell>
                    {
                        new BoardCell(1,1),
                        new BoardCell(2,1),
                        new BoardCell(3,1)
                    }
                }
            };
        }
    }
}
