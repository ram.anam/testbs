﻿using Battleship.Tracker.Models;
using Shouldly;
using Xunit;

namespace Battleship.Tracker.Tests.Models
{
    public class BattleShipTests
    {
        public BattleShip _sut;

        public BattleShipTests()
        {
            _sut = new BattleShip(
                new BoardCell(1, 1),
                3,
                ShipOrientation.Horizontal_LeftToRight);
        }

        [Fact]
        public void Given_ValidPosition_When_TakeAttack_Should_ReturnHit()
        {
            // Act.
            var attackResult = _sut.Attack(new BoardCell(1, 2));

            // Assert.
            attackResult.ShouldBe(AttackResult.Hit);
        }

        [Fact]
        public void Given_InValidPosition_When_TakeAttack_Should_ReturnMiss()
        {
            // Act.
            var attackResult = _sut.Attack(new BoardCell(1, 4));

            // Assert.
            attackResult.ShouldBe(AttackResult.Miss);
        }

        [Fact]
        public void Given_SameAttackPosition_When_TakeAttack_Should_ReturnMiss()
        {
            // Act.
            _ = _sut.Attack(new BoardCell(1, 2));
            var attackResult2 = _sut.Attack(new BoardCell(1, 2));

            // Assert.
            attackResult2.ShouldBe(AttackResult.Miss);
        }

        [Fact]
        public void Given_AllPositionsAreAttacked_When_IsSunk_ReturnsTrue()
        {
            // Act.
            _ = _sut.Attack(new BoardCell(1, 1));
            _ = _sut.Attack(new BoardCell(1, 2));
            _ = _sut.Attack(new BoardCell(1, 3));

            // Assert.
            _sut.Sunk.ShouldBeTrue();
        }

        [Fact]
        public void Given_AllPositionsAreNotAttacked_When_IsSunk_ReturnsFalse()
        {
            // Act.
            _ = _sut.Attack(new BoardCell(1, 1));
            _ = _sut.Attack(new BoardCell(1, 2));

            // Assert.
            _sut.Sunk.ShouldBeFalse();
        }
    }
}
