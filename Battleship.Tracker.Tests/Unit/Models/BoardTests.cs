﻿using Battleship.Tracker.Models;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Battleship.Tracker.Tests.Models
{
    public class BoardTests
    {
        public Board _sut;

        public BoardTests()
        {
            _sut = new Board(
                new List<BattleShip>
                {
                    new BattleShip(new BoardCell(1,1), 3, ShipOrientation.Horizontal_LeftToRight),
                    new BattleShip(new BoardCell(1,5), 4, ShipOrientation.Vertical_BottomToTop)
                });
        }

        [Theory]
        [InlineData(1,1)]
        [InlineData(1,2)]
        [InlineData(1,3)]
        [InlineData(1,5)]
        [InlineData(2,5)]
        [InlineData(3,5)]
        [InlineData(4,5)]
        public void Given_ValidAttack_When_TakeAttack_Should_Return_Hit(int row, int column)
        {
            // Act.
            var attackResult = _sut.Attack(new BoardCell(row, column));

            // Assert.
            attackResult.ShouldBe(AttackResult.Hit);
        }

        [Fact]
        public void Given_AllPositionsAttacked_When_HasLost_Returns_True()
        {
            // Arrange.
            var attacks = new List<BoardCell>
            { 
                new BoardCell(1,1),
                new BoardCell(1,2),
                new BoardCell(1,3),
                new BoardCell(1,5),
                new BoardCell(2,5),
                new BoardCell(3,5),
                new BoardCell(4,5)
            };

            foreach (var eachAttack in attacks)
                _sut.Attack(eachAttack);

            // Assert.
            _sut.HasLost().ShouldBeTrue();
        }

        [Fact]
        public void Given_AllPositionsAreNotAttacked_When_HasLost_Returns_False()
        {
            // Arrange.
            var attacks = new List<BoardCell>
            {
                new BoardCell(1,1),
                new BoardCell(1,2),
                new BoardCell(1,3),
                new BoardCell(1,5),
                new BoardCell(2,5),
                new BoardCell(3,5),
            };

            foreach (var eachAttack in attacks)
                _sut.Attack(eachAttack);

            // Assert.
            _sut.HasLost().ShouldBeFalse();
        }
    }
}
