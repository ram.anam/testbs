﻿using Battleship.Tracker.Models;
using Battleship.Tracker.Validators;
using FluentValidation.TestHelper;
using Shouldly;
using System.Collections.Generic;
using Xunit;

namespace Battleship.Tracker.Tests.Validators
{
    public class BoardValidatorTests
    {
        private readonly BoardValidator _sut;

        public BoardValidatorTests()
        {
            _sut = new BoardValidator();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Given_Invalid_RowCount_When_Validate_Should_Have_Error(int rowCount)
        {
            // Arrange.
            var ship = new BattleShip(new BoardCell(1, 1), 3, ShipOrientation.Horizontal_LeftToRight);

            var board = new Board(new List<BattleShip> { ship }, rowCount);

            // Act.
            var validationResults = _sut.TestValidate(board);

            // Assert.
            validationResults.ShouldHaveValidationErrorFor(b => b.Rows);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Given_Invalid_ColumnCount_When_Validate_Should_Have_Error(int columnCount)
        {
            // Arrange.
            var ship = new BattleShip(new BoardCell(1, 1), 3, ShipOrientation.Horizontal_LeftToRight);

            var board = new Board(new List<BattleShip> { ship }, columns: columnCount);

            // Act.
            var validationResults = _sut.TestValidate(board);

            // Assert.
            validationResults.ShouldHaveValidationErrorFor(b => b.Columns);
        }

        [Fact]
        public void Given_Ship_Outside_Bounds_Of_Board_When_Validate_Should_Have_Error()
        {
            // Arrange
            var ship1 = new BattleShip(new BoardCell(1, 1), 3, ShipOrientation.Horizontal_LeftToRight);
            var ship2 = new BattleShip(new BoardCell(1, 3), 4, ShipOrientation.Vertical_BottomToTop); ;

            var board = new Board(new List<BattleShip> { ship1, ship2 }, 3, 5);

            // Act.
            var validationResults = _sut.TestValidate(board);

            // Assert.
            validationResults.ShouldHaveValidationErrorFor(b => b.Ships);
            validationResults
                .Errors[0]
                .AttemptedValue
                .ShouldBe(ship2);
        }
    }
}
