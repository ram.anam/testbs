﻿using Battleship.Tracker.Models;
using Shouldly;
using System.Collections.Generic;
using Xunit;

namespace Battleship.Tracker.Tests.Integration
{
    public class BattleShipSimulatorTest
    {
        [Fact]
        public void Given_Series_Of_Actions_Board_Should_Return_Valid_Status()
        {
            // Requirement - Create a board and add BattleShip to the board
            var ship1 = new BattleShip(new BoardCell(1, 1), 3, ShipOrientation.Horizontal_LeftToRight);
            var ship2 = new BattleShip(new BoardCell(1, 5), 3, ShipOrientation.Vertical_BottomToTop);

            var board = new Board(new List<BattleShip> { ship1, ship2 });

            // Take an attack.
            var attack1 = board.Attack(new BoardCell(1, 1));
            attack1.ShouldBe(AttackResult.Hit);

            var attack2 = board.Attack(new BoardCell(1, 4));
            attack2.ShouldBe(AttackResult.Miss);

            board.HasLost().ShouldBeFalse();

            // Attack all positions.
            var remainingPositions = new List<BoardCell>
            {
                new BoardCell(1,2),
                new BoardCell(1,3),
                new BoardCell(1,5),
                new BoardCell(2,5),
                new BoardCell(3,5),
            };

            remainingPositions.ForEach(pos => board.Attack(pos));

            board.HasLost().ShouldBeTrue();
        }
    }
}
